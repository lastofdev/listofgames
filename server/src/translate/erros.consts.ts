export const ERROR_MESSAGE = {
    not_valid_game_id: 'Неверный id пользователя',
    not_valid_limit: 'Неверный limit',
    not_valid_offset: 'Неверный offset',
    not_found_games: 'Игры не найдены',
    not_found_game: 'Игрa не найденa',
    server_error: 'Ошибка сервера',

}