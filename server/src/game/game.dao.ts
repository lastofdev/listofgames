import db from '../database/devdb.js'
import { CustomError } from '../error/customError'
import { ERROR_MESSAGE } from '../translate/erros.consts'

type FilterType = {
    limit: number,
    offset: number,
}

class categoryShopDao {
    async findAllGames(filter: FilterType) {
        const games = await db.query('SELECT * FROM game LIMIT $1 OFFSET $2', [filter.limit, filter.offset])

        if (games.rowCount < 1) {
            throw new CustomError(ERROR_MESSAGE.not_found_games)
        }

        return games.rows
    }

    async findGame(game_id: string) {
        const game = await db.query('SELECT * FROM game WHERE game_id = $1', [game_id])

        if (game.rowCount < 1) {
            throw new CustomError(ERROR_MESSAGE.not_found_games)
        }

        return game.rows[0]
    }
}

export default new categoryShopDao()