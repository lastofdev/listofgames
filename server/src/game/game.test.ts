import request from 'supertest'
import { app } from '../../index'
import { ERROR_MESSAGE } from '../translate/erros.consts'
import db from '../database/devdb.js'

const get_game_url = '/game'

describe(get_game_url, () => {
    beforeEach(() => {
        return db.query('START TRANSACTION')
    });

    afterEach(() => {
        return db.query('ROLLBACK')
    });

    it('not valid parameters get games', async () => {
        const games_config = {
            offset: 'test',
            limit: 'test',
        }

        await request(app)
            .put(get_game_url)
            .send(games_config)
            .set('accept', 'application/json')
            .set('Content-Type', 'application/json')
            .expect(400, {
                message: [
                    ERROR_MESSAGE.not_valid_offset, ERROR_MESSAGE.not_valid_limit]
            })
    })

    it('success get all games', async () => {
        const games_config = {
            offset: 0,
            limit: 10,
        }

        await request(app)
            .put(get_game_url)
            .send(games_config)
            .set('accept', 'application/json')
            .set('Content-Type', 'application/json')
            .expect(200)
            .then((response) => {
                expect(response.body.data).toBeTruthy()
                expect(response.body.data).toHaveLength(10)
            })
    })

})

describe(get_game_url, () => {
    beforeEach(() => {
        return db.query('START TRANSACTION')
    });

    afterEach(() => {
        return db.query('ROLLBACK')
    });

    it('not valid parameters game_id', async () => {
        const game_id = 'test_id'

        await request(app)
            .get(`${get_game_url}/${game_id}`)
            .set('accept', 'application/json')
            .set('Content-Type', 'application/json')
            .expect(400, {
                message: [ERROR_MESSAGE.not_valid_game_id]
            })
    })

    it('success get game', async () => {

        const game_id = '6527422a-d38b-4113-8345-334dc6ffb5e6'

        await request(app)
            .get(`${get_game_url}/${game_id}`)
            .set('accept', 'application/json')
            .set('Content-Type', 'application/json')
            .expect(200)
            .then((response) => {
                expect(response.body.data).toBeTruthy()
                expect(response.body.data.game_id).toBeTruthy()
                expect(response.body.data.game_id).toEqual(game_id)
                expect(response.body.data.name).toBeTruthy()
                expect(response.body.data.description).toBeTruthy()
            })
    })


})