function sortByRating(array) {
    if (array.length > 1) {
        return [...array].sort((a, b) => a.rating - b.rating)
    }
    return array
}

function sortByName(array) {
    if (array.length > 1) {
        return [...array].sort((a, b) => {
            const nameA = a.name.toLowerCase()
            const nameB = b.name.toLowerCase()

            if (nameA < nameB) {
                return -1;
            }
            if (nameA > nameB) {
                return 1;
            }

            return 0;
        })
    }
    return array
}

export function sortBy(type, array) {
    if (type === null || type === undefined) {
        return array
    }

    if (type === 'RATING') {
        return sortByRating(array)
    }

    if (type === 'NAME') {
        return sortByName(array)
    }
}