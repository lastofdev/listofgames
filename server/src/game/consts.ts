import crypto from 'node:crypto'

export type GameType = {
    game_id: string,
    name: string,
    description: string,
    rating: number,
    img: string
}

export const MOCK_DATA_GAMES: GameType[] = [
    {
        game_id: crypto.randomUUID(),
        name: 'Game',
        description: 'description game',
        rating: 5,
        img: '/game-photo',
    },
    {
        game_id: crypto.randomUUID(),
        name: 'Honor',
        description: 'description game',
        rating: 3,
        img: '/game-photo-1',
    },
    {
        game_id: crypto.randomUUID(),
        name: 'Lol',
        description: 'description game',
        rating: 4,
        img: '/game-photo-2',
    },
    {
        game_id: crypto.randomUUID(),
        name: 'Dota casino',
        description: 'description game',
        rating: 5,
        img: '/game-photo-3',
    },
    {
        game_id: crypto.randomUUID(),
        name: 'PUBG',
        description: 'description game',
        rating: 3,
        img: '/game-photo-4',
    },
    {
        game_id: crypto.randomUUID(),
        name: 'Slots 757',
        description: 'description game',
        rating: 1,
        img: '/game-photo-5',
    },
    {
        game_id: crypto.randomUUID(),
        name: 'Super game',
        description: 'description game',
        rating: 5,
        img: '/game-photo-6',
    },
    {
        game_id: crypto.randomUUID(),
        name: 'Mario',
        description: 'description game',
        rating: 5,
        img: '/game-photo-7',
    },
    {
        game_id: crypto.randomUUID(),
        name: 'Banana',
        description: 'description game',
        rating: 5,
        img: '/game-photo-8',
    },
    {
        game_id: crypto.randomUUID(),
        name: 'Stels',
        description: 'description game',
        rating: 2,
        img: '/game-photo-9',
    },
    {
        game_id: crypto.randomUUID(),
        name: 'Ultra',
        description: 'description game',
        rating: 2,
        img: '/game-photo-10',
    },
    {
        game_id: crypto.randomUUID(),
        name: 'Anti game',
        description: 'description game',
        rating: 1,
        img: '/game-photo-11',
    },
    {
        game_id: crypto.randomUUID(),
        name: 'Test game',
        description: 'description game',
        rating: 2,
        img: '/game-photo-12',
    },
    {
        game_id: crypto.randomUUID(),
        name: 'pro Game',
        description: 'description game',
        rating: 3,
        img: '/game-photo-13',
    },
    {
        game_id: crypto.randomUUID(),
        name: 'virus Game',
        description: 'description game',
        rating: 4,
        img: '/game-photo-14',
    },
]