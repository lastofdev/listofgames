import { logger } from '../../logger'
import { ERROR_MESSAGE } from '../translate/erros.consts'
import { Request, Response } from 'express'
import { CustomError } from '../error/customError'
import gameDao from './game.dao'
import { sortBy } from './helpers'


class GameService {
    async getGames(req: Request, res: Response) {
        try {
            logger.info('/game > ' + req.body)
            const { limit, offset, sort_by } = req.body

            const filter_config = {
                limit: limit,
                offset: offset,
            }
            const games = await gameDao.findAllGames(filter_config)

            return res.status(200).json({ data: sortBy(sort_by, games), message: null })

        } catch (e) {
            if (e instanceof CustomError) {
                logger.warn('/game > error > ' + e.message)
                return res.status(400).json({ message: e.message })
            }

            logger.error(`/game > error > ${e}`)
            return res.status(500).json({ message: ERROR_MESSAGE.server_error })
        }
    }

    async getGame(req: Request, res: Response) {
        try {
            logger.info('/game/:game_id > ' + req.params.game_id)
            const game = await gameDao.findGame(req.params.game_id)

            return res.status(200).json({ data: game, message: null })

        } catch (e) {
            if (e instanceof CustomError) {
                logger.warn('/game/:game_id > error > ' + e.message)
                return res.status(400).json({ message: e.message })
            }

            logger.error(`/game/:game_id > error > ${e}`)
            return res.status(500).json({ message: ERROR_MESSAGE.server_error })
        }
    }

}

export default new GameService()