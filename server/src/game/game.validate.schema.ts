import { body, param } from 'express-validator'
import { ERROR_MESSAGE } from '../translate/erros.consts'

export const getGame = [
    param('game_id').
        isUUID(4).withMessage(ERROR_MESSAGE.not_valid_game_id),
]

export const getGames = [
    body('offset').
        exists().withMessage(ERROR_MESSAGE.not_valid_offset).bail().
        isNumeric().withMessage(ERROR_MESSAGE.not_valid_offset),
    body('limit').
        exists().withMessage(ERROR_MESSAGE.not_valid_limit).bail().
        isNumeric().withMessage(ERROR_MESSAGE.not_valid_limit),
]