import { Router } from 'express'
import GameService from './game.service'
import { validate } from '../middleware/validateMiddleware'
import { getGame, getGames } from './game.validate.schema'

const gameRouter = Router()

gameRouter.put('/', validate(getGames), GameService.getGames)
gameRouter.get('/:game_id', validate(getGame), GameService.getGame)

export default gameRouter


