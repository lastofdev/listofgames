import { matchedData, validationResult } from 'express-validator';
import { logger } from '../../logger'

export function validate(schemas) {
    return async (req, res, next) => {

        await Promise.all(schemas.map((schema) => schema.run(req)));

        const errorFormatter = ({ location, msg, param, value, nestedErrors }) => {
            return msg;
        };

        const result = validationResult(req).formatWith(errorFormatter);

        if (result.isEmpty()) {
            return next();
        }

        const errors = result.array();

        logger.warn(`${req.path} > validate > errors > ${errors} `)
        return res.status(400).send({ message: errors })
    };
}