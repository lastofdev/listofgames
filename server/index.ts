import express, { Application, NextFunction, Request, Response } from 'express'
import cors from 'cors'
import gameRouter from './src/game/game.routes'

export const app: Application = express();

app.use(cors())
app.use(express.json())
app.use(express.static('public'))

app.use('/game', gameRouter)

const PORT = process.env.PORT || 3001;

app.get('/', (req: Request, res: Response, next: NextFunction) => {
    res.send('Express server with TypeScript');
});

if (process.env.NODE_ENV !== 'test') {
    app.listen(PORT, () => console.log("Server is running... localhost:3001"))
}
