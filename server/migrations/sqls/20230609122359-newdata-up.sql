/* Create table game*/
CREATE TABLE IF NOT EXISTS game (
    game_id UUID PRIMARY KEY,
    name VARCHAR(255) UNIQUE NOT NULL,
    description VARCHAR(255) NOT NULL,
    image_link VARCHAR(255) NOT NULL,
    rating INTEGER NOT NULL,
    created_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    last_update TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP
);

CREATE UNIQUE INDEX IF NOT EXISTS game_game_id_key ON game(game_id);

/* Insert data in table game */
INSERT INTO
    game (game_id, name, description, image_link, rating)
VALUES
    (
        '6527422a-d38b-4113-8345-334dc6ffb5e6',
        'Slot',
        'description 1',
        '/link1',
        1
    ),
    (
        '92081a72-0248-404d-b3e3-71f5f1b6261a',
        'Dota',
        'description 2',
        '/link2',
        2
    ),
    (
        '5de1a01f-e204-4c7c-80bf-71146bbd73d9',
        'Uncharted',
        'description 3',
        '/link3',
        2
    ),
    (
        'e879bafe-d3f5-4f17-b0b9-1a31d2d462b2',
        'CS',
        'description 4',
        '/link4',
        3
    ),
    (
        'a41e208b-3b25-4e38-827b-53c61d66250f',
        'Banana',
        'description 5',
        '/link5',
        3
    ),
    (
        '08579bb1-9251-4e12-beb9-23e33ab8a63c',
        'PUBG',
        'description 6',
        '/link6',
        4
    ),
    (
        '6bd09559-a633-4d1a-aef7-3c9d7ac9c347',
        'Mario',
        'description 7',
        '/link7',
        4
    ),
    (
        '3e8e4918-319c-4592-bfef-8fbc2b85ac89',
        'Stels',
        'description 8',
        '/link8',
        5
    ),
    (
        'b092f390-ef99-4fd3-9a6a-eaacdb0eec35',
        'Anti-game',
        'description 9',
        '/link9',
        5
    ),
    (
        'af51cd4a-5a65-4581-b690-0a8850dea48a',
        'Virus Game',
        'description 10',
        '/link10',
        3
    ),
    (
        '7d3e9052-4869-4ec9-931c-b30efc85efae',
        'Flash',
        'description 11',
        '/link11',
        2
    ),
    (
        'fa04a83b-aa68-4a37-8eea-422f744fa9f1',
        'Mojjo',
        'description 12',
        '/link12',
        2
    ),
    (
        'e5e8e306-55af-481c-b142-25b9fd43fad9',
        'Generator',
        'description 13',
        '/link13',
        2
    ),
    (
        '2ee896f1-747c-459d-9b73-dfffd4ef1d0f',
        'Node',
        'description 14',
        '/link14',
        1
    ),
    (
        'e82bb5bd-748c-4a86-8925-64deb661dcb3',
        'Berry',
        'description 15',
        '/link15',
        5
    );