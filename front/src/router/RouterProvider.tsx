import { GameList } from 'pages/GameList/GameList';
import React from 'react';
import {
    Route,
    Routes,
    useNavigate,
} from 'react-router-dom';

export function RouterProvider() {

    return (
        <Routes>
            <Route path="/" element={<GameList />} />
            {/* <Route path="/register-shop" element={<RegisterShop />} /> */}
        </Routes>
    );
}