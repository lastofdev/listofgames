import * as React from 'react';
import InputLabel from '@mui/material/InputLabel';
import MenuItem from '@mui/material/MenuItem';
import FormControl from '@mui/material/FormControl';
import Select, { SelectChangeEvent } from '@mui/material/Select';


export type Item = {
  title: string;
  value: string;
}

type Props = {
  onChange?: (item: any) => void;
  defaultValue?: string;
  data: Item[];
  label: string;
  id?: string;
  required?: boolean;
  name?: string;
}

export function DropDown(props: Props) {
  const [value, setValue] = React.useState(props.defaultValue ? props.defaultValue : '');

  function handleChange(event: SelectChangeEvent) {
    if (event.target.value) {
      setValue(event.target.value)
      props.onChange(event.target)
    }
  };

  return (
    <FormControl size="small" style={{ minWidth: 180 }}>
      <InputLabel id={props.id}>{props.label}</InputLabel>
      <Select
        required={props.required}
        labelId={props.id}
        id={props.id}
        value={value}
        name={props.name}
        label={props.label}
        defaultValue={props.defaultValue}
        onChange={handleChange}
      >
        {props.data.map((item) => {
          return (
            <MenuItem value={item.value} key={item.value}>
              {item.title}
            </MenuItem>
          )
        })}
      </Select>
    </FormControl>
  );
}
