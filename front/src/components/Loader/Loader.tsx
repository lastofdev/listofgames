import styles from './Loader.module.scss';

export function Loader(): JSX.Element {
    return (
        <div className={styles["gooey"]}>
            <span className={styles["dot"]}></span>
            <div className={styles["dots"]}>
                <span></span>
                <span></span>
                <span></span>
            </div>
        </div>
    );
}
