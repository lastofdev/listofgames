import { ICONS } from './Icons';

export type IconType = string;

type Props = {
    name: IconType | string | any;
}

export function Icon(props: Props) {
    return ICONS[props.name];
}
