import styles from './Card.module.scss'
import classnames from 'classnames'
import { RandomAvatar } from "react-random-avatars"


type Props = {
    id: string,
    title: string,
    photo: string,
    onClick: (id: string) => void,
    type: string
}

export function Card(props: Props): JSX.Element {

    function handleChange(): void {
        props.onClick(props.id)
    }

    const cardClassNames = classnames(
        styles['card'],
        styles[`card_${props.type}`],
    );

    return (
        <div className={cardClassNames} onClick={handleChange}>
            <RandomAvatar name={props.title} size={200} />
            <div className={styles['title']}>{props.title}</div>
        </div>
    );
}

