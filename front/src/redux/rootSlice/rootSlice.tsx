import { createSlice, createAsyncThunk } from '@reduxjs/toolkit'
import { getGames, getGameById } from './rest'

const LIMIT_STEP = 10

export const fetchGames = createAsyncThunk('root/fetchGames',
    async function (_: void, { rejectWithValue, getState }) {
        const state = getState() as any;
        try {
            const result = await getGames(state.root.filters)

            if (result?.status !== 200) {
                return rejectWithValue('Ошибка код не 200')
            }

            return result.response.data
        }

        catch (err) {
            console.log('fetchSignup/error', err)
            return rejectWithValue(err)
        }
    })


export const fetchGameInfo = createAsyncThunk('root/fetchLogin',
    async function (id: string, { rejectWithValue }) {
        try {
            const result = await getGameById(id)

            if (result?.status !== 200) {
                return rejectWithValue('Ошибка код не 200')
            }

            console.log('result', result)
            return result.response.data
        }

        catch (err) {
            console.log('fetchLogin/error', err)
            return rejectWithValue(err)
        }
    }
)

export type GameType = {
    game_id: string,
    name: string,
    description: string,
    rating: number,
    img: string
}


type FilterType = {
    sort_by: 'RATING' | 'NAME' | null,
    limit: number,
    offset: number,
}

export interface RootState {
    isLoading: boolean,
    display: string,
    filters: FilterType
    games: GameType[],
    game: GameType | null,
    error: string | null;
}

const initialState: RootState = {
    isLoading: false,
    games: [],
    game: null,
    display: 'small',
    filters: {
        sort_by: null,
        limit: 10,
        offset: 0,
    },
    error: null,
}


export const rootSlice = createSlice({
    name: 'root',
    initialState,
    reducers: {
        updateDisplay: (state, action) => {
            state.display = action.payload;
        },
        updateSort: (state, action) => {
            state.filters.sort_by = action.payload;
        },
        updateLimit: (state) => {
            state.filters.limit = state.filters.limit + LIMIT_STEP;
        },
    },
    extraReducers: (builder) => {
        // fetchGames
        builder.addCase(fetchGames.pending, (state) => {
            state.isLoading = true
        })
        builder.addCase(fetchGames.fulfilled, (state, action) => {
            state.games = action.payload;
            state.isLoading = false;
        })
        builder.addCase(fetchGames.rejected, (state, action) => {
            state.isLoading = false;
        })

        // fetchGameInfo
        builder.addCase(fetchGameInfo.pending, (state) => {
            state.isLoading = true;
        })
        builder.addCase(fetchGameInfo.fulfilled, (state, action) => {
            state.game = action.payload;
            state.isLoading = false;
        })
        builder.addCase(fetchGameInfo.rejected, (state, action) => {
            state.isLoading = false;
        })
    },
})

export const { updateSort, updateLimit, updateDisplay } = rootSlice.actions
export default rootSlice.reducer
