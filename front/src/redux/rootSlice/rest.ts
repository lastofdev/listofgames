export async function getGames(body) {
    try {
        const response = await fetch('http://localhost:3001/game', {
            method: 'PUT',
            headers: {
                'accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(body)
        });

        const data = await response.json();

        return {
            response: data,
            status: response.status
        }

    } catch (error) {
        console.log('Error fetch', error)
    }
}

export async function getGameById(game_id) {
    try {
        const result = await fetch(`http://localhost:3001/game/${game_id}`, {
            method: 'GET',
            headers: {
                'accept': 'application/json',
                'Content-Type': 'application/json',
            }
        });

        const data = await result.json();

        return {
            response: data,
            status: result.status
        }

    } catch (error) {
        console.log('Error fetch', error)
    }
}

