import React from 'react'
import styles from './GameList.module.scss'
import { Dashboard } from './components/Dashboard/Dashboard'
import { Filters } from './components/Filters/Filters'
import { PopupInfoGame } from './components/PopupInfoGame/PopupInfoGame'

export function GameList(): JSX.Element {
    const [showPopup, setShowPopup] = React.useState(false);
    const [gameId, setGameId] = React.useState(null);

    function openPopupInfoGame(id) {
        setGameId(id)
        setShowPopup(true)
    }

    function closePopupCategory() {
        setShowPopup(false)
    }

    return (
        <>
            <div className={styles['wrapper']}>
                <Filters title='All games' />
                <Dashboard onOpen={openPopupInfoGame} />
            </div>
            {showPopup && (
                <PopupInfoGame id={gameId} onClose={closePopupCategory} />
            )}
        </>
    );
}

