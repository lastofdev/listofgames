import React from 'react'
import styles from './PopupInfoGame.module.scss';
import { Modal } from 'components/Modal/Modal';
import { Loader } from 'components/Loader/Loader';
import { useAppDispatch, useAppSelector } from 'redux/hooks'
import { RandomAvatar } from "react-random-avatars";
import { fetchGameInfo } from 'redux/rootSlice/rootSlice';

type Props = {
    id: string;
    onClose?: () => void;
}

export function PopupInfoGame(props: Props) {
    const dispatch = useAppDispatch()
    const { game, isLoading } = useAppSelector((state) => state.root)

    React.useEffect(() => {
        dispatch(fetchGameInfo(props.id))
    }, [])

    if (isLoading || game === null) {
        return <Loader />
    }

    return (
        <Modal title='Информация о игре' onClose={props.onClose}>
            <div className={styles['info']}>
                <div>  <RandomAvatar name={game.name} size={200} /></div>
                <span>Название: <b>{game.name}</b></span>
                <span>Описание: <b>{game.description}</b></span>
                <span>Рейтинг: <b>{game.rating}/5</b></span>
            </div>
        </Modal>
    );
}

