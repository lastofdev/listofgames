export const DROPDOWN_DISPLAY = [
    {
        title: 'Большой размер',
        value: 'large'
    },
    {
        title: 'Маленький размер',
        value: 'small'
    },
]

export const DROPDOWN_SORT = [
    {
        title: 'По названию',
        value: "NAME"
    },
    {
        title: 'По рейтингу',
        value: "RATING"
    },
]