import styles from './Filters.module.scss'
import { DropDown, Item } from 'components/DropDown/DropDown'
import { DROPDOWN_DISPLAY, DROPDOWN_SORT } from './consts'
import { fetchGames, updateDisplay, updateSort } from 'redux/rootSlice/rootSlice'
import { useAppDispatch, useAppSelector } from 'redux/hooks'

type Props = {
    title: string,
}

export function Filters(props: Props): JSX.Element {
    const { filters, display } = useAppSelector((state) => state.root)
    const dispatch = useAppDispatch()

    function handleChangeDisplay(item: Item) {
        dispatch(updateDisplay(item.value))
    }

    function handleChangeSort(item: Item) {
        dispatch(updateSort(item.value))
        dispatch(fetchGames())
    }

    return (
        <div className={styles['wrapper']}>
            <div className={styles['title']}>{props.title}</div>
            <div className={styles['filters']}>
                <DropDown
                    onChange={handleChangeDisplay}
                    data={DROPDOWN_DISPLAY}
                    defaultValue={display}
                    label='Отображение'
                    name='display'
                />
                <DropDown
                    onChange={handleChangeSort}
                    data={DROPDOWN_SORT}
                    defaultValue={filters.sort_by}
                    label='Сортировка'
                    name='sort'
                />
            </div>
        </div>
    );
}

