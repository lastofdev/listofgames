import React from 'react'
import styles from './Dashboard.module.scss'
import { Card } from 'components/Card/Card'
import { Button } from 'components/Button/Button'
import { useAppDispatch, useAppSelector } from 'redux/hooks'
import { fetchGames, updateLimit } from 'redux/rootSlice/rootSlice'
import { Loader } from 'components/Loader/Loader'
import { MAX_LENGTH_GAMES } from './consts'

type Props = {
    onOpen: (item: string) => void,
}

export function Dashboard(props: Props): JSX.Element {
    const { isLoading, games, display } = useAppSelector((state) => state.root)
    const dispatch = useAppDispatch()

    function openPopupInfoGame(id) {
        props.onOpen(id)
    }

    function handleClickPagination() {
        dispatch(updateLimit())
        dispatch(fetchGames())
    }

    React.useEffect(() => {
        dispatch(fetchGames())
    }, [])

    if (isLoading) {
        return <Loader />
    }

    return (
        <div className={styles['wrapper']}>
            <div className={styles['list']}>
                {games.map((item) => {
                    return <Card
                        photo={item.name}
                        title={item.name}
                        key={item.game_id}
                        id={item.game_id}
                        type={display}
                        onClick={openPopupInfoGame} />
                })}
            </div>
            <div className={styles['button']}>
                {games.length > MAX_LENGTH_GAMES ? <div>Все данные загружены</div> :
                    <Button type='light' onClick={handleClickPagination}>Показать еще</Button>}
            </div>
        </div>
    );
}

