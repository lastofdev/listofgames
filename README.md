## Test task - Game Dashboard

Необходимо реализовать отображение списка игр в виде карточек.

### Backend

- Реализовать REST API с помощью которого можно будет получить списко игр и расширенную информацию по игре. В качестве хранилища можно использовать любую БД.
- При запросе списка игр возвращается следующая информация: id, название, иконка игры.
- При запросе расширенной информации по игре возвращается: id, название, иконка игры, описание, рейтинг популярности игры.

### Frontend

- Необходиом реализовать отображение игр в виде карточек, на каждой карточке есть информация о названии игры, иконка. При клике открывается popup с более подробной информацией по игре: название, картинка, описание и рейтинг.
- Список игр отображается по 10 елементов на странице с воможностью перехода по страницам (пагинация).
- В списке игр должна присутвовать возможность сортировки по имени и рейтингу.

### Стили
Необходимо реализовать возможность выбора стиля отображения карточек, на выбор должны доступны два стиля, которые поменяют внешний вид списка игр. В стилизации предоставляется полная свобода.

# Настройка проекта

## install Node.js, npm and PostgreSQL

- Node version > 18.16 (https://nodejs.org/en/download)
- npm version > 8
- PostgreSQL version > 14 (https://www.postgresql.org/download/)

# Getting started Front

### `cd front`

### `npm install`

- install all node_modules for project

### `npm run start`

- start react app on port 3000

# Getting started Back

### `cd servrer`

### `npm install`

- install all node_modules for project

# PostgreSQL create db

- createuser postgres
- createdb test_game_db
- host localhost
- password 1994
- port 5432


### `db-migrate up newdata`

- migrate data in db test_game_db

### `npm run start`

- start server on port 3001
